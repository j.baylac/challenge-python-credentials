# Challenge 03 Python : Credentials

Cette semaine, il s'agit d'un challenge à réaliser individuellement. 
Vous pouvez bien évidemment demander de l'aide à vos collègues.

Vous disposez du fichier ```accounts.csv``` contenant des comptes utilisateurs ainsi que leurs scores au sein d'un jeu. 

user|password|score_0|score_1|score_2|score_3
--- | --- | --- |--- | --- | ---
|pseudo1|mot_de_passe|0|0|0|0
|pseudo2|pwd2|10|7|3|4
|pseudo3|pwd3|8|5|9|6



Un fichier CSV peut être généré à partir d'une page Excel. 
Ce qui va stocker, au sein d'un fichier plat, les informations ligne par ligne. Séparant les colones par un séparateur.
Ce séparateur est souvent ```;``` ou ```,``` en fonction des préférences régionales de l'ordinateur.

Ces fichiers contiennent souvent, en première ligne, l'entête des colones.


Chaque exercice contient, en commentaire, des instructions.
Votre objectif étant de parcourir ce fichier pour en extraire certaines informations.

Conseil: 
Si vous buttez sur un exercice 
Ecrivez l'algo en commentaire ou faites un schéma BPML. 

Un rendu est attendu pour Mardi 30 Octobre.

## Exercice 1 :
Réaliser la fonction user_exists qui valide le fait que le compte utilisateur existe.
Cette fonction doit retourner vrai si l'utilisateur existe et faux dans le cas contraire.

```python challenge.userExists.py```

## Exercice 2
Réaliser la fonction login qui valide la connection d'un utilisateur.
Cette fonction prend deux paramètres en entrée : le nom de l'utilisateur et son mot de passe.
```python challenge.login.py```

## Exercice 3
Réaliser la fonction average qui donne la moyenne des scores d'un compte

```python challenge.average.py```

## Exercice 4
Réaliser une fonction qui calcule le score le plus faible parmi tous les utilisateurs.
Et une autre fonction pour celui qui est le plus fort.
```python challenge.minmax.py```

## Exercice 5
Réaliser une fonction qui liste les scores la plus faible et le plus fort, regroupés par utilisateur
```python challenge.minmaxuser.py```

## Exercice 6
Relisez votre code, ces fonctions peuvent-elles s'appeler entre elles? 
Si oui, faites en sorte que ce soit le cas.
Ecrire les tests unitaires correspondant aux fonctions des niveaux précédents.


## Exercice 7
Ecrivez un nouveau script python. 
Et déroulez le scénario de votre choix demandant à l'utilisateur de s'authentifier et d'accéder à ses informations.
